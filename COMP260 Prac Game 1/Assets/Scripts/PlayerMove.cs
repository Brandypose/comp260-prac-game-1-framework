﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

	public Vector2 move;
	public Vector2 velocity; // in metres per second
	public float maxSpeed = 5.0f;
	public float acceleration = 3.0f; // in metres/second/second
	public float brake = 5.0f; // in metres/second/second
	private float speed = 0.0f;    // in metres/second


	// Use this for initialization
	void Start () {
		
	}

	
	// Update is called once per frame
	void Update () {
		
		// the vertical axis controls acceleration fwd/back
		float forwards = Input.GetAxis ("Vertical");
		if (forwards > 0) {
			// accelerate forwards
			speed = speed + acceleration * Time.deltaTime;
		} else if (forwards < 0) {
			// accelerate backwards
			speed = speed - acceleration * Time.deltaTime;
		}

		else {
			// braking
			if (speed > 0) {
				speed = speed - brake * Time.deltaTime;
			} else {
				speed = speed + brake * Time.deltaTime;
			}
		}

		// clamp the speed
		speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);

		// compute a vector in the up direction of length speed
		Vector2 velocity = Vector2.up * speed;

		// move the object
		transform.Translate (velocity * Time.deltaTime);
	}
}
