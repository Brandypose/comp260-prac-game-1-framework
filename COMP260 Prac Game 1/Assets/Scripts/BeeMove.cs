﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {

	public float speed = 4.0f;        // metres per second
	public float turnSpeed = 180.0f;  // degrees per second
	public Transform target;
	public Transform target2;
	public Vector2 direction;
	public Vector2 heading = Vector3.right; 


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		// calculate how much to turn per frame
		float angle = turnSpeed * Time.deltaTime;

		// get the vector from the bee to the target 
		// Vector2 direction = target.position - transform.position;

		Vector2 direction = transform.position - target.position;

		var compare1 = Mathf.Abs((target.position - transform.position).magnitude);
		Debug.Log ("Distance from player 1 to bee" + compare1);

		var compare2 = Mathf.Abs((target2.position - transform.position).magnitude);
		Debug.Log ("Distance from player 2 to bee" + compare2);


		// Distance from the first vector to the second
		// float dis = Vector3.Distance (target, target2);
		// Distance from the second vector to the first
		// float factor = Vector3.Distance (target2, target);

		// compare which one is smaller here

		if(compare1 < compare2) {
			// change heading towards dis
			direction = target.position - transform.position;
		} else {
			// change heading towards factor
			direction = target2.position - transform.position;
		}

		// turn left or right
		if (direction.IsOnLeft(heading)) {
			// target on left, rotate anticlockwise
			heading = heading.Rotate(angle);
		}
		else {
			// target on right, rotate clockwise
			heading = heading.Rotate(-angle);
		}

		transform.Translate(heading * speed * Time.deltaTime);

	}

}

//compare magnitude (distance) of vectors, move toward the shorter one